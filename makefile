SRC=main.c
OUT=rsa

debug:
	gcc $(SRC) -g -Wall -Wextra -fsanitize=leak,address,undefined -o $(OUT)
