#include <stdio.h>
#include <string.h>
#include <stdint.h>

uint16_t
prim(uint16_t a, uint16_t b)
{
	static const uint16_t c[] = { 1, 0 };
	a = a + c[a & 1];
	b = b - c[b & 1];

	for (; a <= b; a += 2) {
		for (uint16_t i = 3; i < (a / 2); ++i)
			if (a % i == 0)
				goto _PRIM_CONT_;	
		return a;
_PRIM_CONT_:
		continue;
	}

	return 0;
}

uint16_t
gcd(uint16_t a, uint16_t b)
{
	// gcd(a, b) = gcd(b, mod(a, b))
	
	uint16_t r;
	do {
		r = a % b;
		a = b;
		b = r;
	} while (r != 0);

	return a;
}

uint16_t
eea(uint16_t r0, uint16_t r1)
{
	// (d * e) mod phi = 1
	// r_i = s * r_0 + t * r_1
	uint16_t phi;
	phi = r0;
	
	int_fast32_t t0, t1;
	t0 = 0;
	t1 = 1;

	int_fast32_t q, r, t;
	while (r1 != 0) {
		q = r0 / r1;
		r = r0 % r1;
	
		t = t0 - q * t1;

		r0 = r1;
		r1 = r;

		t0 = t1;	
		t1 = t;
	}

	while (t0 < 0)
		t0 += phi;

	return (uint16_t)t0;
}

uint16_t
sqml(uint16_t x, uint16_t k, uint16_t n)
{
	int c;
	c  = __builtin_clz(k);
	// c is an int which is twice as long as k.
	c -= 16;
	k <<= c;

	uint_fast64_t r;
	r = x;
	for (k <<= 1; k != 0; k <<= 1) {
		r = (r * r) % n;
		if (k & 0x8000)
			r = (r * x) % n;
		r = r % n;
	}
	printf("R: %lu\n", r);

	return (uint16_t)r;
}

int
main(void)
{
	uint16_t p, q;
	p = prim(10, 15);
	q = prim(16, 20);

	printf("p: %u, q: %u\n", p, q);

	uint16_t n;
	n = p * q;

	printf("n: %u\n", n);

	uint16_t phi;
	phi = (p - 1) * (q - 1);

	printf("phi: %u\n", phi);

	uint16_t e;
	e = 153;

	printf("e: %u, gcd(e, phi) = %u\n", e, gcd(e, phi));

	uint16_t d;
	d = eea(phi, e);

	printf("d: %u\n", d);

	printf("KEY_PUB: (%u, %u)\n", e, n);
	printf("KEY_PRV: (%u, %u)\n", d, n);

	uint16_t x;
	x = 0x2;

	printf("x:    0x%04X\n", x);

	uint16_t r;
	r = sqml(x, e, n);
	printf("e(x): 0x%04X\n", r);

	printf("d(x): 0x%04X\n", sqml(r, d, n));

	return 0;
}
